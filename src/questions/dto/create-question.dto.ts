import { IsArray, IsInt, Min, Max, IsString } from 'class-validator';

export class CreateQuestionDto {
  @IsString()
  question: string;

  @IsArray()
  choices: string[];

  @IsInt()
  @Min(0)
  @Max(3)
  answerIndex: number;
}
