declare var process: {
  env: {
    QUESTIONS_PER_QUIZ: number;
  };
};

export const questionConstants: { numberOfQuestions: number } = {
  numberOfQuestions: process.env.QUESTIONS_PER_QUIZ || 5,
};
