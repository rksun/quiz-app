import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateQuestionDto } from './dto/create-question.dto';
import { UpdateQuestionDto } from './dto/update-question.dto';
import { Question, QuestionDocument } from './schemas/question.schema';

@Injectable()
export class QuestionsService {
  constructor(
    @InjectModel(Question.name) private questionModel: Model<QuestionDocument>,
  ) {}

  async create(createQuestionDto: CreateQuestionDto): Promise<Question> {
    const createdQuestion = new this.questionModel(createQuestionDto);
    return createdQuestion.save();
  }

  async findAll(): Promise<Question[]> {
    return this.questionModel.find().exec();
  }

  async findOne(id: string): Promise<Question> {
    return this.questionModel.findOne({ _id: id });
  }

  async update(id: string, updateQuestionDto: UpdateQuestionDto) {
    return this.questionModel.updateOne(
      { _id: id },
      { $set: { ...updateQuestionDto } },
    );
  }

  async remove(id: string) {
    return this.questionModel.deleteOne({ _id: id });
  }

  async getAvailableQuestions() {
    return this.questionModel.find({ inUse: false }).exec();
  }

  async updateQuestionsStatus(questionIds: string[], state: boolean) {
    return this.questionModel.updateMany(
      { _id: { $in: questionIds } },
      { $set: { inUse: state } },
    );
  }
}
