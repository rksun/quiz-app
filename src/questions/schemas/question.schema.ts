import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type QuestionDocument = Question & Document;

@Schema()
export class Question {
  @Prop()
  question: string;

  @Prop([String])
  choices: string[];

  @Prop()
  answerIndex: number;

  @Prop({ default: false })
  inUse: boolean;
}

export const QuestionSchema = SchemaFactory.createForClass(Question);
