import { ConflictException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { User, UserDocument } from './schemas/user.schema';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async findOne(username: string): Promise<User | undefined> {
    return this.userModel.findOne({ username }).exec();
  }

  async findUserById(id: string): Promise<User> {
    return this.userModel.findById(id, { password: 0, __v: 0 });
  }

  async signUp(createUserDto: CreateUserDto): Promise<any> {
    const { username, password, role } = createUserDto;
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = new this.userModel({
      username,
      password: hashedPassword,
      role,
    });
    try {
      const { username, role } = await user.save();
      return { username, role };
    } catch (error) {
      if (error.code === 11000) {
        throw new ConflictException('User already exists');
      }
      throw error;
    }
  }

  async updateUserScore(id: string, scoreObj): Promise<void> {
    const user = await this.userModel.findById(id);
    user.quizScores.push(scoreObj);
    await user.save();
  }

  async getQuizTaken(id: string): Promise<string[]> {
    const user = await this.userModel.findById(id);
    const quizArray = user.quizScores.map((obj) => obj.id);
    return quizArray;
  }
}
