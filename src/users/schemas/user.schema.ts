import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

export type UserScore = {
  id: string;
  score: number;
};

@Schema()
export class User {
  @Prop({ unique: true })
  username: string;

  @Prop()
  password: string;

  @Prop({ default: 'testTaker' })
  role: string;

  @Prop()
  quizScores: UserScore[];
}

export const UserSchema = SchemaFactory.createForClass(User);
