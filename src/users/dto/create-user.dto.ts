import { IsIn, IsOptional, IsString } from "class-validator";

export class CreateUserDto {
  @IsString()  
  username: string;
  
  @IsString()
  password: string;

  @IsOptional()
  @IsIn(['testTaker', 'testSetter'])
  role: string;
}
