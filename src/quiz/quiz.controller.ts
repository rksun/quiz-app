import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
} from '@nestjs/common';
import { QuizService } from './quiz.service';
import { AnswerDto } from './dto/answer.dto';
import { RolesGuard } from 'src/auth/roles.guard';
import { Roles } from 'src/auth/roles.decorator';

@Controller('quiz')
@UseGuards(RolesGuard)
export class QuizController {
  constructor(private readonly quizService: QuizService) {}

  @Post('/create')
  @Roles('testSetter')
  createQuiz() {
    return this.quizService.createQuiz();
  }

  @Get()
  findAll(@Request() req) {
    const userId = req.user._id;
    const role = req.user.role;
    return this.quizService.findAll(userId, role);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.quizService.findOne(id);
  }

  @Post(':id/submit')
  @Roles('testTaker')
  submitAnswers(
    @Request() req,
    @Param('id') id: string,
    @Body() answerDto: AnswerDto,
  ) {
    const userId = req.user._id;
    return this.quizService.submitAnswers(id, userId, answerDto);
  }

  @Delete(':id')
  @Roles('testSetter')
  remove(@Param('id') id: string) {
    return this.quizService.remove(id);
  }
}
