import { Type } from 'class-transformer';
import { IsArray, IsNumber, IsString, ValidateNested } from 'class-validator';

export class Answer {
  @IsString()  
  id: string;
  @IsNumber()
  answerIndex: number;
}

export class AnswerDto {
  @ValidateNested({ each: true })
  @Type(() => Answer)
  @IsArray()  
  answers: Answer[];
}
