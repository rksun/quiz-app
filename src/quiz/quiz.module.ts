import { Module } from '@nestjs/common';
import { QuizService } from './quiz.service';
import { QuizController } from './quiz.controller';
import { QuestionsModule } from 'src/questions/questions.module';
import { Quiz, QuizSchema } from './schemas/quiz.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from 'src/auth/auth.module';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    QuestionsModule,
    MongooseModule.forFeature([{ name: Quiz.name, schema: QuizSchema }]),
    AuthModule,
    UsersModule,
  ],
  controllers: [QuizController],
  providers: [QuizService],
})
export class QuizModule {}
