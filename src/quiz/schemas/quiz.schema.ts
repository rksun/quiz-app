import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Question, QuestionDocument, QuestionSchema } from 'src/questions/schemas/question.schema';

export type QuizDocument = Quiz & Document;

@Schema()
export class Quiz {
  @Prop({ type: [QuestionSchema]})
  questionSet: QuestionDocument[];
}

export const QuizSchema = SchemaFactory.createForClass(Quiz);
