import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Quiz, QuizDocument } from './schemas/quiz.schema';
import { AnswerDto } from './dto/answer.dto';
import { questionConstants } from 'src/questions/constants';
import { UsersService } from 'src/users/users.service';
import { QuestionsService } from 'src/questions/questions.service';

function shuffleArr(array: any[]): void {
  for (var i = array.length - 1; i > 0; i--) {
    var rand = Math.floor(Math.random() * (i + 1));
    [array[i], array[rand]] = [array[rand], array[i]];
  }
}

@Injectable()
export class QuizService {
  constructor(
    @InjectModel(Quiz.name) private quizModel: Model<QuizDocument>,
    private usersService: UsersService,
    private questionsService: QuestionsService,
  ) {}
  async createQuiz() {
    // set the number of questions per quiz
    const { numberOfQuestions } = questionConstants;
    // find all questions that are not in use
    const availableQuestions =
      await this.questionsService.getAvailableQuestions();
    if (availableQuestions.length < numberOfQuestions) {
      throw new BadRequestException('Not enough questions');
    }
    // shuffle the array to create randomized quiz
    shuffleArr(availableQuestions);
    // each quiz should have "numberOfQuestions" questions
    const numberOfQuiz = Math.floor(
      availableQuestions.length / numberOfQuestions,
    );
    let quizIds = [];
    // create quiz with "numberOfQuestions" questions each
    for (let i = 0; i < numberOfQuiz; i++) {
      const questionSet = availableQuestions.splice(0, numberOfQuestions);
      const createdQuiz = new this.quizModel();
      createdQuiz.questionSet = questionSet;
      const questionIds = questionSet.map((element) => element._id);
      // update the status of the corresponding questions
      await this.questionsService.updateQuestionsStatus(questionIds, true);
      // collect the ids of the created quiz
      await createdQuiz.save().then((obj) => quizIds.push(obj._id));
    }

    return { numberOfQuizCreated: numberOfQuiz, ids: quizIds };
  }

  async findAll(userId: string, role: string) {
    // if testTaker, return list of quiz ids not yet taken by user, else return all
    if (role === 'testTaker') {
      const quizTaken = await this.usersService.getQuizTaken(userId);
      const allQuiz = await this.quizModel.find(
        { _id: { $nin: quizTaken } },
        { _id: 1 },
      );
      const results = allQuiz.map((obj) => obj._id);
      return { availableQuiz: results };
    }
    return this.quizModel.find().select('-__v -questionSet.inUse').exec();
  }

  async findOne(id: string): Promise<Quiz> {
    return this.quizModel.findById(id, {
      'questionSet.question': 1,
      'questionSet.choices': 1,
      'questionSet._id': 1,
    });
  }

  async submitAnswers(id: string, userId: string, answerDto: AnswerDto) {
    // check if user has already taken the quiz
    const quizTaken = await this.usersService.getQuizTaken(userId);
    if (quizTaken.includes(id)) {
      throw new BadRequestException('This quiz has already been taken');
    }
    let score: number = 0;
    const quiz = await this.quizModel.findById(id);
    if (!quiz) {
      throw new NotFoundException('Quiz does not exist');
    }
    const questionSet = quiz.questionSet;
    // a map to hold the answerIndex for the corresponding question id
    const answerMap = new Map();
    for (let question of questionSet) {
      answerMap.set(String(question['_id']), question['answerIndex']);
    }
    // convert map to object for display in results; stackoverflow solution
    const mapToObj = (m: Iterable<unknown> | ArrayLike<unknown>) => {
      return Array.from(m).reduce((obj, [key, value]) => {
        obj[key] = value;
        return obj;
      }, {});
    };

    const correctAnswers = mapToObj(answerMap);

    // calculate the score
    for (let answer of answerDto.answers) {
      if (answerMap.has(answer.id)) {
        if (answerMap.get(answer.id) === answer.answerIndex) score += 1;
        // remove entry from the map to prevent duplicates
        answerMap.delete(answer.id);
      }
    }
    const scoreObj = { id, score };
    // update user score
    await this.usersService.updateUserScore(userId, scoreObj);
    return {
      numberOfQuestions: questionSet.length,
      score: score,
      correctAnswers: correctAnswers,
    };
  }

  async remove(id: string) {
    // update the status of the corresponding questions in this quiz
    const quiz = await this.quizModel.findById(id);
    if (!quiz) {
      throw new NotFoundException('Quiz does not exist');
    }
    const { questionSet } = quiz;
    const questionIds = questionSet.map((element) => element._id);
    await this.questionsService.updateQuestionsStatus(questionIds, false);
    return this.quizModel.deleteOne({ _id: id });
  }
}
