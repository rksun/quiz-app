Project Setup
=============

Three environment variables need to be set:
-  `MONGO_URI` pointing to a mongodb instance
- `JWT_SECRET` for authentication purposes
- `QUESTIONS_PER_QUIZ`(optional) to set number of questions per quiz

The application may then be started by running either of the following commands from the project root:
- `npm run start:dev` for watch mode
- `npm run start:prod` for production mode

Endpoints
=========

The following endpoints are available:

| Endpoint | Method | Accessibility | Description
| --- | ----------- | ------------- | -----------
|/auth/signup| POST | Public | Required fields: `username`, `password`. An optional `role` field may be passed with either `testSetter` or `testTaker`(default)
|/auth/login| POST | Public | Required fields: `username`, `password`. Returns an `access_token` which is the bearer token required for the private endpoints
|/auth/profile| GET | Private | Returns basic user information including `username`, `role` and `quizScores`
|/questions| GET | Private(testSetter) | Retrieves all the questions
|/questions| POST | Private(testSetter) | Required fields: `question`, `choices`(an array of 4 possible answers), `answerIndex`(the array index of the correct answer)
|/questions/:id| GET | Private(testSetter) | Retrieves the question with ID of `id`
|/questions/:id| PATCH | Private(testSetter) | Edit the question with ID of `id`. Accepted fields: `question`, `choices` and `answerIndex`
|/questions/:id| DELETE | Private(testSetter) | Deletes the question with ID of `id`
|/quiz/create| POST | Private(testSetter) | Creates a quiz with the available questions. The number of questions per quiz is determined by the environment variable `QUESTIONS_PER_QUIZ`(defaults to 5)
|/quiz| GET | Private | Returns all the available quizzes with questions, choices and answers for `testSetter` or just a list of quiz IDs for `testTaker`(excludes quizzes already taken by the user)
|/quiz/:id| GET | Private | Returns the quiz with `id` without the answers
|/quiz/:id| DELETE | Private(testSetter) | Deletes the quiz with ID of `id`
|/quiz/:id/submit| POST | Private(testTaker) | Required fields: An array of objects containing `id` and `answerIndex`. The `id` here is the ID of the individual questions. The `:id` in the endpoint refers to the quiz ID